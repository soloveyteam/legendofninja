﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostPlayerTrigger : MonoBehaviour
{
    [SerializeField] private GhostController ghostController;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            ghostController.isPlayerReadyToTalk = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            ghostController.isPlayerReadyToTalk = false;
        }
    }
}
