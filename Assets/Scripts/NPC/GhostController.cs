﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostController : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private GameObject ghost;
    [SerializeField] private GameObject ghostImageForScale;
    [SerializeField] private Transform currentPoint;
    [SerializeField] private Transform[] points;
    [SerializeField] private int pointSelection;
    private bool isChangeDirectionReady = true;
    public bool isPlayerReadyToTalk = false;

    void Start()
    {
        currentPoint = points[pointSelection];
    }

    private void Update()
    {
        if (!isPlayerReadyToTalk)
        {
            ghost.transform.position = Vector3.MoveTowards(ghost.transform.position, currentPoint.position, Time.deltaTime * speed);

            Vector3 direction = currentPoint.position - ghost.transform.position;
            if (ghostImageForScale.transform.localScale.x < 0)
            {
                if (direction.x < 0)
                    ghostImageForScale.transform.localScale = Vector3.one;
            }
            else
            {
                if (direction.x > 0)
                    ghostImageForScale.transform.localScale = new Vector3(-1f, 1f, 1f);
            }

            if ((ghost.transform.position == currentPoint.position) && isChangeDirectionReady)
            {
                Invoke("ChangeDirection", 0f);
                isChangeDirectionReady = false;
            }
        }
    }

    private void ChangeDirection()
    {
        pointSelection++;

        if (pointSelection == points.Length)
        {
            pointSelection = 0;
        }

        currentPoint = points[pointSelection];

        isChangeDirectionReady = true;
    }

    
}
