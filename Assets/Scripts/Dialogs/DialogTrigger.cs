﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogTrigger : MonoBehaviour
{
    [SerializeField] private GameObject dialogCanvas;
    [SerializeField] private GameObject startDialogCanvasOnNPC;
    [SerializeField] private Dialog dialog;
    private bool isPlayerReadyToTalk = false;
    private Text startDialogImageText;

    private void Update()
    {
        if (isPlayerReadyToTalk && Input.GetKeyDown(KeyCode.E))
        {
            //Debug.Log("Клавиша Е нажата (до запуска TriggerDialog())");
            TriggerDialog();
            //Debug.Log("Клавиша Е нажата (до после TriggerDialog())");
        }
    }

    public void TriggerDialog()
    {
        dialogCanvas.transform.Find("DialogArea").gameObject.SetActive(true);
        startDialogCanvasOnNPC.SetActive(false);
        FindObjectOfType<DialogManager>().StartDialog(dialog);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();

        if (player != null)
        {
            isPlayerReadyToTalk = true;
            startDialogCanvasOnNPC.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();

        if (player != null)
        {
            isPlayerReadyToTalk = false;
            startDialogCanvasOnNPC.SetActive(false);
            dialogCanvas.transform.Find("DialogArea").gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        if (startDialogCanvasOnNPC != null)
        {
            startDialogCanvasOnNPC.SetActive(false);
        }

        if (dialogCanvas != null)
        {
            dialogCanvas.transform.Find("DialogArea").gameObject.SetActive(false);
        }
    }
}
