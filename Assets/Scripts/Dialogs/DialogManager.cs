﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogText;

    private string nameOfPlayer;
    private string nameOfNPC;

    private Queue<string> sentences;

    void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartDialog(Dialog dialog)
    {
        StopAllCoroutines();

        nameOfPlayer = dialog.nameOfPlayer;
        nameOfNPC = dialog.nameOfNPC;

        sentences.Clear();

        foreach (string sentence in dialog.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialog();
            return;
        }

        string sentence = sentences.Dequeue();

        if (sentence[0] == 'P')
        {
            nameText.text = nameOfPlayer;
        }
        else
        {
            nameText.text = nameOfNPC;
        }

        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogText.text = "";
        sentence = sentence.TrimStart(new char[] { 'P', 'N' });
        foreach (char letter in sentence.ToCharArray())
        {
            dialogText.text += letter;
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void EndDialog()
    {
        Debug.Log("End dialog.");
        //if (nameOfNPC == "Призрак")
        //{
        //    // Тут нужно запустить картиночку, что получили амулет.
        //    Amulet itemData = new Amulet();
            
        //    itemData.ItemType = InventoryItemType.Amulet;

        //    itemData.AmuletType = AmuletType.ManaRecovery;

        //    GameController.Instance.AddInventoryItem(itemData);

        //    GameController.Instance.KillNpc(nameOfNPC);
        //}

        //if (gameObject.name == "CookNpc")
        //{
        //    //GameController.Instance.KillNpc(nameOfNPC);
        //    // Телепортировать игрока к босу, то есть загрузить третью сцену.
        //}

        switch (nameOfNPC)
        {
            case "Призрак":
                {
                    Amulet itemData = new Amulet();

                    itemData.ItemType = InventoryItemType.Amulet;

                    itemData.AmuletType = AmuletType.ManaRecovery;

                    GameController.Instance.AddInventoryItem(itemData);

                    GameController.Instance.KillNpc(nameOfNPC);
                }
                break;

            case "Приветливый повар":
                // Телепортировать игрока к босу, то есть загрузить третью сцену.
                break;
        }
    }
}
