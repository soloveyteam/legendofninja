﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataStore
{
    [SerializeField] private static List<InventoryItem> savedInventory;
    private static HeroParameters savedHero;
    //private int lives = GameController.Instance.Lives;
    //private float savedMaxHealth = GameController.Instance.Hero.MaxHealth;
    //private float savedMaxMana = GameController.Instance.Hero.MaxMana;
    //private float savedDamage = GameController.Instance.Hero.Damage;
    //private float savedSpeed = GameController.Instance.Hero.Speed;
    //private int savedExperience = GameController.Instance.Hero.Experience;
    //private int savedLevel = GameController.Instance.Hero.Level;

    private static int savedLives;
    private static float savedMaxHealth;
    private static float savedMaxMana;
    private static float savedDamage;
    private static float savedSpeed;
    private static int savedExperience;
    private static int savedLevel;
    private static int savedStatPoints;
    private static int savedNextExperienceLevel;
    private static int savedPreviousExperienceLevel;
    private static bool savedIsAmuletOfSpeedOn;
    private static bool savedIsAmuletOfManaRecoveryOn;

    private static int primordialLives = 3;
    private static float primordialMaxHealth = 100f;
    private static float primordialMaxMana = 100f;
    private static float primordialDamage = 20f;
    private static float primordialSpeed = 5f;
    private static int primordialExperience = 0;
    private static int primordialLevel = 1;
    private static int primordialStatPoints = 0;
    private static int primordialNextExperienceLevel = 100;
    private static int primordialPreviousExperienceLevel = 0;

    public void SaveInventory()
    {
        if (savedInventory == null)
        {
            savedInventory = new List<InventoryItem>();
        }

        savedInventory.Clear();
        for (int i = 0; i < GameController.Instance.Inventory.Count; i++)
        {
            savedInventory.Add(GameController.Instance.Inventory[i]);
        }
    }

    public void LoadSavedInventory()
    {
        if (savedInventory == null)
        {
            savedInventory = new List<InventoryItem>();
        }

        GameController.Instance.Inventory.Clear();
        for (int i = 0; i < savedInventory.Count; i++)
        {
            GameController.Instance.Inventory.Add(savedInventory[i]);
        }
    }

    public void SaveHeroParameters()
    {
        savedLives = GameController.Instance.Lives;
        savedMaxHealth = GameController.Instance.Hero.MaxHealth;
        savedMaxMana = GameController.Instance.Hero.MaxMana;
        savedDamage = GameController.Instance.Hero.Damage;
        savedSpeed = GameController.Instance.Hero.Speed;
        savedExperience = GameController.Instance.Hero.Experience;
        savedLevel = GameController.Instance.Hero.Level;
        savedStatPoints = GameController.Instance.Hero.StatPoints;
        savedNextExperienceLevel = GameController.Instance.Hero.NextExperienceLevel;
        savedPreviousExperienceLevel = GameController.Instance.Hero.PreviousExperienceLevel;

        savedIsAmuletOfSpeedOn = GameController.Instance.IsAmuletOfSpeedOn;
        savedIsAmuletOfManaRecoveryOn = GameController.Instance.IsAmuletOfManaRecoveryOn;
    }

    public void LoadSavedHeroParameters()
    {
        GameController.Instance.Lives = savedLives;
        GameController.Instance.Hero.MaxHealth = savedMaxHealth;
        GameController.Instance.Hero.MaxMana = savedMaxMana;
        GameController.Instance.Hero.Damage = savedDamage;
        GameController.Instance.Hero.Speed = savedSpeed;
        GameController.Instance.Hero.Experience = savedExperience;
        GameController.Instance.Hero.Level = savedLevel;
        GameController.Instance.Hero.StatPoints = savedStatPoints;
        GameController.Instance.Hero.NextExperienceLevel = savedNextExperienceLevel;
        GameController.Instance.Hero.PreviousExperienceLevel = savedPreviousExperienceLevel;

        GameController.Instance.IsAmuletOfSpeedOn = savedIsAmuletOfSpeedOn;
        GameController.Instance.IsAmuletOfManaRecoveryOn = savedIsAmuletOfManaRecoveryOn;
    }

    public void LoadPrimordialHeroParametersAndInventory()
    {
        GameController.Instance.Lives = primordialLives;
        GameController.Instance.Hero.MaxHealth = primordialMaxHealth;
        GameController.Instance.Hero.MaxMana = primordialMaxMana;
        GameController.Instance.Hero.Damage = primordialDamage;
        GameController.Instance.Hero.Speed = primordialSpeed;
        GameController.Instance.Hero.Experience = primordialExperience;
        GameController.Instance.Hero.Level = primordialLevel;
        GameController.Instance.Hero.StatPoints = primordialStatPoints;
        GameController.Instance.Hero.NextExperienceLevel = primordialNextExperienceLevel;
        GameController.Instance.Hero.PreviousExperienceLevel = primordialPreviousExperienceLevel;
        GameController.Instance.Inventory.Clear();

        GameController.Instance.IsAmuletOfSpeedOn = false;
        GameController.Instance.IsAmuletOfManaRecoveryOn = false;
        
    }
}
