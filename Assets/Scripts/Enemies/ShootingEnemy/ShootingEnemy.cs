﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : MonoBehaviour
{
    [SerializeField] private Transform fireballLauncher;
    [SerializeField] private float shootingDelay = 1f;
    private FireballEnemy fireball;

    private void Awake()
    {
        // Подгружаем ссылку на префаб "FireballEnemy"
        fireball = Resources.Load<FireballEnemy>("Prefabs/Fireball/Fireball_enemy");
    }

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine("ShootFireball");
    }


    private void CreateFireball()
    {
        Vector3 position = fireballLauncher.transform.position;
        FireballEnemy newFireball = Instantiate(fireball, position, fireball.transform.rotation) as FireballEnemy;
        newFireball.Owner = gameObject;
        //newFireball.Direction = newFireball.transform.right * (isPlayerLookingToRight ? 1.0f : -1.0f);
        newFireball.Direction = fireballLauncher.right;
        newFireball.transform.rotation = fireballLauncher.rotation;
    }

    private IEnumerator ShootFireball()
    {
        while(true)
        {
            CreateFireball();
            yield return new WaitForSeconds(shootingDelay);
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
