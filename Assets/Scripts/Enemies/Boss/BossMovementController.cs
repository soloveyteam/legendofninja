﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovementController : MonoBehaviour
{
    
    [SerializeField] private float speed = 1f;
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject bossImageForScale;
    [SerializeField] private Transform currentPoint;
    [SerializeField] private Transform[] points;
    [SerializeField] private int pointSelection;
    private bool isChangeDirectionReady = true;
    private bool isPlayerReadyToFight = true;

    void Start()
    {
        currentPoint = points[pointSelection];
    }

    void Update()
    {
        if (isPlayerReadyToFight)
        {
            boss.transform.position = Vector3.MoveTowards(boss.transform.position, currentPoint.position, Time.deltaTime * speed);

            Vector3 direction = currentPoint.position - boss.transform.position;
            //if (bossImageForScale.transform.localScale.x < 0)
            //{
            //    if (direction.x < 0)
            //        bossImageForScale.transform.localScale = Vector3.one;
            //}
            //else
            //{
            //    if (direction.x > 0)
            //        bossImageForScale.transform.localScale = new Vector3(-1f, 1f, 1f);
            //}

            if ((boss.transform.position == currentPoint.position) && isChangeDirectionReady)
            {
                Invoke("ChangeDirection", 0f);
                isChangeDirectionReady = false;
            }
        }
    }

    private void ChangeDirection()
    {
        pointSelection++;

        if (pointSelection == points.Length)
        {
            pointSelection = 0;
        }

        currentPoint = points[pointSelection];

        isChangeDirectionReady = true;
    }

    private void OnDestroy()
    {
        HUD.Instance.ShowLevelWonWindow();
    }
}
