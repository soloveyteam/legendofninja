﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEyesMovementController : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private Transform centerOfWhiteofEye;
    private Transform eyeTransform;

    void Start()
    {
        eyeTransform = transform;
    }

    void Update()
    {
        eyeTransform.position = centerOfWhiteofEye.position + (player.transform.position - centerOfWhiteofEye.position).normalized * 0.15f;
    }
}
