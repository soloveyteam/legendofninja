﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttacksController : MonoBehaviour
{
    [SerializeField] private Transform[] minionLaunchers;
    private BossMinionsController minion;

    private void Start()
    {
        minion = Resources.Load<BossMinionsController>("Prefabs/Enemies/BossMinion");
        StartCoroutine("MinionsLauncher");
    }

    private IEnumerator MinionsLauncher()
    {
        for ( ; ; )
        {
            yield return new WaitForSeconds(5f);

            for (int i = 0; i < 4; i++)
            {
                CreateMinion(i);
            }
        }
    }

    private void CreateMinion(int i)
    {
        Vector3 position = minionLaunchers[i].transform.position;
        BossMinionsController newMinion = Instantiate(minion, position, minion.transform.rotation) as BossMinionsController;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            IDestructable destructable = collision.gameObject.GetComponent<IDestructable>();
            if (destructable != null)
            {
                destructable.RecieveHit(20.0f);
            }
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
