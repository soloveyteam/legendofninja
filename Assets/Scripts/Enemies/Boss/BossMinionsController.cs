﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMinionsController : MonoBehaviour
{
    private float speed = 5.0f;
    private Vector3 direction;
    private float damage = 5.0f;


    void Start()
    {
        direction = GameObject.FindGameObjectWithTag("Player").transform.position - transform.position;
        direction = direction.normalized;

        Destroy(gameObject, 10f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            IDestructable destructable = collision.gameObject.GetComponent<IDestructable>();
            if (destructable != null)
            {                
                destructable.RecieveHit(damage);
                Destroy(gameObject, 0.2f);
            }
        }
        else if (collision.transform.CompareTag("Wall") || collision.transform.CompareTag("Ground"))
        {
            Destroy(gameObject);
        }
    }
}
