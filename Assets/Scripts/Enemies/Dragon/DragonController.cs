﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonController : Creature
{
    [SerializeField] private CircleCollider2D hitCollider;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_animator = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        Vector2 velocity = m_rigidbody.velocity;
        velocity.x = speed * transform.localScale.x * -1;
        m_rigidbody.velocity = velocity;
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        PlayerController player = collider.gameObject.GetComponent<PlayerController>();
        if ((player != null) && !GameController.Instance.IsPlayerDead)
        {
                m_animator.SetTrigger("Attack");
        }
        else if (collider.transform.CompareTag("ChangeDirection"))
        {
            ChangeDirection();
        }
    }

    private void ChangeDirection()
    {
        if (transform.localScale.x < 0)
        {
            transform.localScale = Vector3.one;
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    public void Attack()
    {
        Vector3 hitPosition = transform.TransformPoint(hitCollider.offset);
        DoHit(hitPosition, hitCollider.radius, Damage);
    }
}
