﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAttack : MonoBehaviour
{
    private DragonController dragonController;
    void Start()
    {
        dragonController = GetComponentInParent<DragonController>();
    }

    public void Attack()
    {
        dragonController.Attack();
    }
}
