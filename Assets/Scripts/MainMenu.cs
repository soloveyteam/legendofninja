﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject optionsWindow;
    [SerializeField] private GameObject controlWindow;
    [SerializeField] private Slider soundSlider;
    [SerializeField] private Slider musicSlider;

    private void Start()
    {
        Time.timeScale = 1.0f;
        GameController.Instance.AudioManager.PlayMusic(true);
        GameController.Instance.SavedDataStore.LoadPrimordialHeroParametersAndInventory();
        SetSliderValue(soundSlider, GameController.Instance.AudioManager.SfxVolume);
        SetSliderValue(musicSlider, GameController.Instance.AudioManager.MusicVolume);
    }

    public void PlayButton()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
        //GameController.Instance.AudioManager.PlayMusic(false);
        GameController.Instance.AudioManager.PlayMusicLvlNumber(1);
    }

    public void OptionsButton()
    {
        ShowWindow(optionsWindow);
    }

    public void ControlButton()
    {
        ShowWindow(controlWindow);
    }

    public void ExitButton()
    {
        Application.Quit();
    }

    public void ShowWindow(GameObject window)
    {
        window.GetComponent<Animator>().SetBool("Open", true);
        GameController.Instance.State = GameState.Pause;
    }

    public void HideWindow(GameObject window)
    {
        window.GetComponent<Animator>().SetBool("Open", false);
        GameController.Instance.State = GameState.Play;
    }

    public void SetSoundVolume(Slider slider)
    {
        GameController.Instance.AudioManager.SfxVolume = slider.value;
    }

    public void SetMusicVolume(Slider slider)
    {
        GameController.Instance.AudioManager.MusicVolume = slider.value;
    }

    private void SetSliderValue(Slider slider, float value)
    {
        slider.value = value;
    }
}
