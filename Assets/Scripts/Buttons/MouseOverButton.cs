﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseOverButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Text buttonText;
    private string startString;
    private string newString;

    void Start()
    {
        buttonText = GetComponentInChildren<Text>();
        if (buttonText != null)
        {
            startString = buttonText.text;
            newString = "< " + buttonText.text + " >";
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (newString != "")
            buttonText.text = newString;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (startString != "")
            buttonText.text = startString;
    }
}
