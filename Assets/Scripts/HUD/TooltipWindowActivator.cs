﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TooltipWindowActivator : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    private GameObject tooltipWindow;
    private RectTransform tooltipWindowRectTransform;
    private Text tooltipText;
    private Image imageTooltipWindow;
    private Color visibleColorOfText;
    private Color invisibleColorOfText;
    private Color visibleColorOfImage;
    private Color invisibleColorOfImage;
    private TooltipLibrary tooltipContent = new TooltipLibrary();
    private string tagOfObject = "";

    private void Start()
    {
        //tooltipContent = new TooltipLibrary();

        visibleColorOfText = Color.white;
        visibleColorOfText.a = 1f;

        invisibleColorOfText = Color.white;
        invisibleColorOfText.a = 0f;

        tooltipWindow = GameObject.FindGameObjectWithTag("TooltipWindow");
        tooltipWindowRectTransform = tooltipWindow.GetComponent<RectTransform>();
        tooltipText = tooltipWindow.GetComponentInChildren<Text>();
        imageTooltipWindow = tooltipWindow.GetComponent<Image>();

        visibleColorOfImage = Color.white;
        invisibleColorOfImage = visibleColorOfImage;
        invisibleColorOfImage.a = 0f;

        tooltipText.color = invisibleColorOfText;
        imageTooltipWindow.color = invisibleColorOfImage;
    }

    private void Update()
    {
        SetTransforPosition();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (gameObject.tag != "Untagget")
        {
            tagOfObject = gameObject.tag;
        }
        else
        {
            tagOfObject = "";
        }
        SetTooltipText();
        tooltipText.color = visibleColorOfText;
        imageTooltipWindow.color = visibleColorOfImage;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltipText.color = invisibleColorOfText;
        imageTooltipWindow.color = invisibleColorOfImage;
    }

    private void SetTooltipText()
    {
        tooltipText.text = tooltipContent.GetTooltipContent(tagOfObject);
    }

    private void SetTransforPosition()
    {
        tooltipWindow.transform.position = new Vector2(Input.mousePosition.x + (tooltipWindowRectTransform.rect.width / 2),
            Input.mousePosition.y - (tooltipWindowRectTransform.rect.height / 2));
    }

    private void OnDestroy()
    {
        tooltipText.color = invisibleColorOfText;
        imageTooltipWindow.color = invisibleColorOfImage;
    }
}
