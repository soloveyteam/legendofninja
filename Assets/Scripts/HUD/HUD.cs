﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    static private HUD _instance;
    [SerializeField] private Text scoreLabel;
    [SerializeField] private Text livesValue;
    [SerializeField] Slider healthBar;
    [SerializeField] Slider manaBar;
    [SerializeField] GameObject inventoryWindow;
    [SerializeField] GameObject levelWonWindow;
    [SerializeField] GameObject levelLoseWindow;
    [SerializeField] GameObject inGameMenuWindow;
    [SerializeField] GameObject optionsWindow;
    [SerializeField] private InventoryUIButton inventoryItemPrefab;
    [SerializeField] private Transform inventoryContainer;
    [SerializeField] private Transform slotForAmuletSpeed;
    [SerializeField] private Transform slotForAmuletMana;
    [SerializeField] private Transform darkScreenTransform;
    [SerializeField] private Image darkScreenImage;
    [SerializeField] private Text damageValue;
    [SerializeField] private Text healthValue;
    [SerializeField] private Text speedValue;
    [SerializeField] private Text playerLevelValue;
    [SerializeField] private Text statPointsValue;
    [SerializeField] private Slider soundSlider;
    [SerializeField] private Slider musicSlider;

    public Text DamageValue
    {
        get { return damageValue; }
        set { damageValue = value; }
    }

    public Text HealthValue
    {
        get { return healthValue; }
        set { healthValue = value; }
    }

    public Text SpeedValue
    {
        get { return speedValue; }
        set { speedValue = value; }
    }

    public Slider HealthBar
    {
        get
        {
            return healthBar;
        }
        set
        {
            healthBar = value;
        }
    }

    public Slider ManaBar
    {
        get
        {
            return manaBar;
        }
        set
        {
            manaBar = value;
        }
    }

    public static HUD Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        //GameController.Instance.SavedDataStore.LoadSavedInventory();
        //Debug.Log("Отображаем в HUD инвентарь из GameController");
        LoadInventory();
        GameController.Instance.OnUpdateHeroParameters += HandleOnUpdateHeroParameters;
        GameController.Instance.OnUpdateHeroMaxHealth += HandleOnUpdateHeroMaxHealth;
        GameController.Instance.OnUpdateHeroCurrentHealth += HandleOnUpdateHeroCurrentHealth;
        GameController.Instance.OnUpdateHeroMana += HandleOnUpdateHeroMana;
        GameController.Instance.StartNewLevel();
        CheckLockedIncreaseButton();
        SetSliderValue(soundSlider, GameController.Instance.AudioManager.SfxVolume);
        SetSliderValue(musicSlider, GameController.Instance.AudioManager.MusicVolume);
    }

    public void SetScore(string scoreValue)
    {
        scoreLabel.text = scoreValue;
    }

    public void SetLivesValue(int lives)
    {
        livesValue.text = lives.ToString();
    }

    public void ShowWindow(GameObject window)
    {
        window.GetComponent<Animator>().SetBool("Open", true);
        GameController.Instance.State = GameState.Pause;
    }

    public void HideWindow(GameObject window)
    {
        window.GetComponent<Animator>().SetBool("Open", false);
        GameController.Instance.State = GameState.Play;
    }

    public InventoryUIButton AddNewInventoryItem(InventoryItem itemData)
    {
        InventoryUIButton newItem = Instantiate(inventoryItemPrefab) as InventoryUIButton;
        newItem.transform.SetParent(inventoryContainer);
        newItem.ItemData = itemData;

        if ((itemData is Amulet))
        {
            if ((((Amulet)itemData).AmuletType == AmuletType.SpeedIncrease))
            {
                newItem.tag = "AmuletSpeedIncrease";
                if (GameController.Instance.IsAmuletOfSpeedOn)
                    newItem.transform.SetParent(slotForAmuletSpeed);
            }
            if ((((Amulet)itemData).AmuletType == AmuletType.ManaRecovery))
            {
                newItem.tag = "AmuletManaRecovery";
                if (GameController.Instance.IsAmuletOfManaRecoveryOn)
                    newItem.transform.SetParent(slotForAmuletMana);
            }
        }
        else if (itemData is Potion)
        {
            switch (((Potion)itemData).PotionType)
            {
                case PotionType.HealthPotion:
                    newItem.tag = "PotionHealth";
                    break;

                case PotionType.ManaPotion:
                    newItem.tag = "PotionMana";
                    break;

                default:
                    Debug.LogError("Wrong potion type!");
                    break;
            }
        }

        return newItem;
    }

    public void AmuletWasUsed(InventoryItem itemData)
    {
        if ((((Amulet)itemData).AmuletType == AmuletType.SpeedIncrease))
        {
            // Это если амулет находится в инвентаре
            for (int i = 0; i < inventoryContainer.childCount; i++)
            {
                GameObject tempChild = inventoryContainer.GetChild(i).gameObject;
                if (tempChild.CompareTag("AmuletSpeedIncrease"))
                {
                    MoveAmuletFromInventoryToSlot(tempChild);
                    return;
                }
            }
            // Если прошли весь инвентарь и не нашли амулета, то рыщем в слотах под амулеты

            if (slotForAmuletSpeed.childCount != 0)
            {
                MoveAmuletFromSlotToInventory(slotForAmuletSpeed.GetChild(0).gameObject);
            }
        }
        else if ((((Amulet)itemData).AmuletType == AmuletType.ManaRecovery))
        {
            // Если амулет находится в инвентаре
            for (int i = 0; i < inventoryContainer.childCount; i++)
            {
                GameObject tempChild = inventoryContainer.GetChild(i).gameObject;
                if (tempChild.CompareTag("AmuletManaRecovery"))
                {
                    MoveAmuletFromInventoryToSlot(tempChild);
                    return;
                }
            }
            // Если прошли весь инвентарь и не нашли амулета, то рыщем в слотах под амулеты

            if (slotForAmuletMana.childCount != 0)
            {
                MoveAmuletFromSlotToInventory(slotForAmuletMana.GetChild(0).gameObject);
            }
        }
    }

    public void MoveAmuletFromInventoryToSlot(GameObject relocatableAmulet)
    {
        //for (int i = 0; i < inventoryContainer.childCount; i++)
        //{
        //    if (inventoryContainer.GetChild(i).transform.Find("LabelText").gameObject.GetComponent<Text>().text == "Speed")
        //    {
        //        inventoryContainer.GetChild(i).transform.SetParent(slotForAmuletSpeed);
        //        break;
        //    }
        //}


        if (relocatableAmulet.CompareTag("AmuletSpeedIncrease"))
        {
            relocatableAmulet.transform.SetParent(slotForAmuletSpeed);
            GameController.Instance.IsAmuletOfSpeedOn = true;
        }
        else if (relocatableAmulet.CompareTag("AmuletManaRecovery"))
        {
            relocatableAmulet.transform.SetParent(slotForAmuletMana);
            GameController.Instance.IsAmuletOfManaRecoveryOn = true;
        }
            
    }

    public void MoveAmuletFromSlotToInventory(GameObject relocatableAmulet)
    {
        if (relocatableAmulet.CompareTag("AmuletSpeedIncrease"))
        {
            GameController.Instance.IsAmuletOfSpeedOn = false;
        }
        else if (relocatableAmulet.CompareTag("AmuletManaRecovery"))
        {
            GameController.Instance.IsAmuletOfManaRecoveryOn = false;
        }

        relocatableAmulet.transform.SetParent(inventoryContainer);

    }

    public void ButtonNext()
    {
        GameController.Instance.LoadNextLevel();
    }
    public void ButtonRestart()
    {
        //DataStore.LoadSavedInventory();
        GameController.Instance.RestartLevel();
    }
    public void ButtonMainMenu()
    {
        GameController.Instance.LoadMainMenu();
    }

    public void ButtonIncreaseDamage()
    {
        GameController.Instance.HeroParameterWasIncreased("Damage");
    }

    public void ButtonIncreaseSpeed()
    {
        GameController.Instance.HeroParameterWasIncreased("Speed");
    }

    public void ButtonIncreaseMaxHealth()
    {
        GameController.Instance.HeroParameterWasIncreased("MaxHealth");
    }

    public void ShowLevelWonWindow()
    {
        ShowWindow(levelWonWindow);
    }

    public void ShowLevelLoseWindow()
    {
        ShowWindow(levelLoseWindow);
    }

    public void ShowDarkScreenForTeleportation()
    {
        StartCoroutine("DarkScreenOn");
    }

    private IEnumerator DarkScreenOn()
    {
        darkScreenTransform.localScale = Vector3.one;
        Color darkColor = Color.black;
        darkColor.a = 0f;

        for (; darkColor.a < 1f;)
        {
            darkColor.a += 0.01f;
            darkScreenImage.color = darkColor;
            //Debug.Log("Прозрачность черного экрана увеличена и тперерь составляет: " + darkScreenImage.color.a);
            yield return new WaitForSeconds(0.01f);
        }
        // Здесь телепортировать
        GameController.Instance.PlayerTeleportationToLastSavePoint();

        StartCoroutine("DarkScreenOff");
        StopCoroutine("DarkScreenOn");
    }

    private IEnumerator DarkScreenOff()
    {
        Color darkColor = Color.black;
        darkColor.a = 1f;

        for (; darkColor.a > 0f;)
        {
            darkColor.a -= 0.01f;
            darkScreenImage.color = darkColor;
            //Debug.Log("Прозрачность черного экрана уменьшена и тперерь составляет: " + darkScreenImage.color.a);
            yield return new WaitForSeconds(0.01f);
        }
        darkScreenTransform.localScale = Vector3.zero;
        StopCoroutine("DarkScreenOff");
    }

    public void LoadInventory()
    {
        InventoryUsedCallback callback = new InventoryUsedCallback(GameController.Instance.InventoryItemUsed);
        for (int i = 0; i < GameController.Instance.Inventory.Count; i++)
        {
            InventoryUIButton newItem = AddNewInventoryItem(GameController.Instance.Inventory[i]);
            newItem.Callback = callback;
        }
    }

    private void HandleOnUpdateHeroParameters(HeroParameters parameters)
    {
        float playerSpeed = parameters.Speed;
        if (GameController.Instance.IsAmuletOfSpeedOn)
            playerSpeed = parameters.Speed * parameters.AmuletSpeedMultiplier;
        //UpdateCharacterValues(parameters.MaxHealth, parameters.Speed, parameters.Damage);
        UpdateCharacterValues(parameters.MaxHealth, playerSpeed, parameters.Damage);

        UpdateCharacterLevelValue(parameters.Level);
        UpdateCharacterStatPointsValue(parameters.StatPoints);
    }

    private void HandleOnUpdateHeroCurrentHealth(HeroParameters parameters)
    {
        HealthBar.value = parameters.MaxHealth;
    }

    private void HandleOnUpdateHeroMaxHealth(HeroParameters parameters)
    {
        HealthBar.maxValue = parameters.MaxHealth;
    }

    private void HandleOnUpdateHeroMana(HeroParameters parameters)
    {
        ManaBar.maxValue = parameters.MaxMana;
        ManaBar.value = parameters.MaxMana;
    }

    public void UpdateCurrentMana(float newCurrentMana)
    {
        ManaBar.value = newCurrentMana;
    }

    public void UpdateCharacterValues(float newHealth, float newSpeed, float newDamage)
    {
        healthValue.text = newHealth.ToString();
        //speedValue.text = newSpeed.ToString();
        speedValue.text = ((float)Math.Round((double)newSpeed, 3)).ToString();
        damageValue.text = newDamage.ToString();
    }

    private void UpdateCharacterLevelValue(int newLevel)
    {
        playerLevelValue.text = newLevel.ToString();
    }

    private void UpdateCharacterStatPointsValue(int newStatPoints)
    {
        statPointsValue.text = newStatPoints.ToString();
    }

    private void OnDestroy()
    {
        GameController.Instance.OnUpdateHeroParameters -= HandleOnUpdateHeroParameters;
        GameController.Instance.OnUpdateHeroMaxHealth -= HandleOnUpdateHeroMaxHealth;
        GameController.Instance.OnUpdateHeroCurrentHealth -= HandleOnUpdateHeroCurrentHealth;
        GameController.Instance.OnUpdateHeroMana -= HandleOnUpdateHeroMana;
    }

    private void CheckLockedIncreaseButton()
    {
        if (GameController.Instance.Hero.Damage >= GameController.Instance.Hero.MaxDamage) LockIncreaseButton("Damage");
        if (GameController.Instance.Hero.Speed >= GameController.Instance.Hero.MaxSpeed) LockIncreaseButton("Speed");
        if (GameController.Instance.Hero.MaxHealth >= GameController.Instance.Hero.MaxMaxHealth) LockIncreaseButton("MaxHealth");
    }

    public void LockIncreaseButton(string currentlyLockedButtonType)
    {
        switch (currentlyLockedButtonType)
        {
            case "Damage":
                inventoryWindow.transform.Find("IncreaseDamageButton").gameObject.GetComponent<Button>().interactable = false;
                inventoryWindow.transform.Find("IncreaseDamageButton").gameObject.GetComponent<Image>().color = Color.green;
                inventoryWindow.transform.Find("DamageLabel").gameObject.GetComponent<Text>().color = Color.green;
                DamageValue.color = Color.green;
                break;

            case "Speed":
                inventoryWindow.transform.Find("IncreaseSpeedButton").gameObject.GetComponent<Button>().interactable = false;
                inventoryWindow.transform.Find("IncreaseSpeedButton").gameObject.GetComponent<Image>().color = Color.green;
                inventoryWindow.transform.Find("SpeedLabel").gameObject.GetComponent<Text>().color = Color.green;
                SpeedValue.color = Color.green;
                break;

            case "MaxHealth":
                inventoryWindow.transform.Find("IncreaseMaxHealthButton").gameObject.GetComponent<Button>().interactable = false;
                inventoryWindow.transform.Find("IncreaseMaxHealthButton").gameObject.GetComponent<Image>().color = Color.green;
                inventoryWindow.transform.Find("HealthLabel").gameObject.GetComponent<Text>().color = Color.green;
                HealthValue.color = Color.green;
                break;

            default:
                Debug.LogError("Wrong parameret in void LockIncreaseButton() (HUD)");
                break;
        }
        
    }

    public void SetSoundVolume(Slider slider)
    {
        GameController.Instance.AudioManager.SfxVolume = slider.value;
    }

    public void SetMusicVolume(Slider slider)
    {
        GameController.Instance.AudioManager.MusicVolume = slider.value;
    }

    private void SetSliderValue(Slider slider, float value)
    {
        slider.value = value;
    }
}
