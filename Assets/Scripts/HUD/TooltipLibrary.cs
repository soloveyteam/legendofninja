﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipLibrary
{
    private Dictionary<string, string> tooltipContent;

    public string GetTooltipContent(string tagOfObject)
    {
        string answer = "";
        tooltipContent = new Dictionary<string, string>
        {
            ["AmuletSpeedIncrease"] = "Амулет ветра \n Дарует носителю \n возможность совершать рывки \n на земле и в воздухе. \n Для активации необходимо надеть.",
            ["AmuletManaRecovery"] = "Амулет восстановления маны \n Для активации необходимо надеть.",
            ["InventoryButton"] = "Инвентарь",
            ["InGameMenuButton"] = "Меню",
            ["PotionHealth"] = "Зелье восстановления здоровья",
            ["PotionMana"] = "Зелье восстановления маны"
        };

        if (tooltipContent.ContainsKey(tagOfObject))
        {
            answer = tooltipContent[tagOfObject];
        }

        return answer;
    }
}
