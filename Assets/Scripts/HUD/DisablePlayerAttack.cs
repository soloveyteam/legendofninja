﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DisablePlayerAttack : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    // Когда мы наводим мышку на кнопку, на которой висит данный скрипт
    // мы запрещаем игроку производить атаку при нажатии на ЛКМ
    public void OnPointerEnter(PointerEventData eventData)
    {
        GameController.Instance.IsMouseOverUIButton = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameController.Instance.IsMouseOverUIButton = false;
    }
}
