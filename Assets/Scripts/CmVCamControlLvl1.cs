﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CmVCamControlLvl1 : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera[] virtualCameras;
    

    private void Start()
    {
        for (int i = 0; i < virtualCameras.Length; i++)
        {
            if (i == 0)
            {
                virtualCameras[i].Priority = 10;
            }
            else
            {
                virtualCameras[i].Priority = 1;
            }
        }
    }

    public void RaiseCameraPriority(int cameraNumber)
    {
        for (int i = 0; i < virtualCameras.Length; i++)
        {
            if (i == cameraNumber - 1)
            {
                virtualCameras[i].Priority = 10;
            }
            else
            {
                virtualCameras[i].Priority = 1;
            }
        }
    }
}
