﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeatController : MonoBehaviour
{
    [SerializeField] private GameObject startDialogCanvasOnNPC;
    private bool isPlayerReadyToEat = false;

    private void Update()
    {
        if (isPlayerReadyToEat && Input.GetKeyDown(KeyCode.E))
        {
            EatMeat();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();

        if (player != null)
        {
            isPlayerReadyToEat = true;
            startDialogCanvasOnNPC.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();

        if (player != null)
        {
            isPlayerReadyToEat = false;
            startDialogCanvasOnNPC.SetActive(false);
        }
    }

    private void EatMeat()
    {
        // Играть звук чавканья
        //GameController.Instance.AudioManager.PlaySoundRandomPitch("Sword_strike");

        // Изменить цвет персонажа
        
        Invoke("PlaySoundToEat1", 0.1f);
    }

    private void PlaySoundToEat1()
    {
        GameController.Instance.AudioManager.PlaySound("Chafk");
        Invoke("PlaySoundToEat2", 1f);
    }

    private void PlaySoundToEat2()
    {
        GameController.Instance.AudioManager.PlaySound("Glotanie");
        SpriteRenderer sprite = FindObjectOfType<PlayerController>().transform.Find("Ninja").GetComponent<SpriteRenderer>();
        StartCoroutine(PlayerSpriteToGreen(sprite));
    }

    private IEnumerator PlayerSpriteToGreen(SpriteRenderer sprite)
    {
        Color currentColor = sprite.color;

        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForSeconds(0.5f);

            switch (i)
            {
                case 0:
                    sprite.color = Color.green;
                    break;

                case 1:
                    sprite.color = Color.yellow;
                    break;

                case 2:
                    sprite.color = Color.black;
                    break;

                case 3:
                    sprite.color = Color.red;
                    break;

                case 4:
                    sprite.color = Color.green;
                    break;
            }
        }
        // Здесь телепортировать
        GameController.Instance.LoadNextLevel();
        
    }

    private void OnDestroy()
    {
        StopCoroutine("PlayerSpriteToGreen");
    }
}
