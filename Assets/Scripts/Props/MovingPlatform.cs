﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private GameObject platform;
    [SerializeField] private Transform currentPoint;
    [SerializeField] private Transform[] points;
    [SerializeField] private int pointSelection;
    private bool isChangeDirectionReady = true;

    void Start()
    {
        currentPoint = points[pointSelection];
    }

    private void Update()
    {
        platform.transform.position = Vector3.MoveTowards(platform.transform.position, currentPoint.position, Time.deltaTime * speed);

        if ((platform.transform.position == currentPoint.position) && isChangeDirectionReady)
        {
            Invoke("ChangeDirection", 1f);
            isChangeDirectionReady = false;
        }
    }

    private void ChangeDirection()
    {
        pointSelection++;

        if (pointSelection == points.Length)
        {
            pointSelection = 0;
        }

        currentPoint = points[pointSelection];

        isChangeDirectionReady = true;
    }
}
