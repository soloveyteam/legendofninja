﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            transform.Find("Mace").GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            transform.Find("Mace").GetComponent<Rigidbody2D>().mass = 30f;
        }
    }
}
