﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PotionType { Random, HealthPotion, ManaPotion }

public class Chest : MonoBehaviour
{
    public Potion itemData = new Potion();
    
    private void OnTriggerEnter2D(Collider2D collider)
    {
        PlayerController player = collider.gameObject.GetComponent<PlayerController>();
        if (player != null)
        {
            //if (itemData.Quantity == 0)
            //{
            //    itemData.Quantity = Random.Range(1, 6);
            //}
            itemData.ItemType = InventoryItemType.Potion;

            //int quantityOfPotionsInChest = Random.Range(1, 4);
            int quantityOfPotionsInChest = 1;

            if (itemData.PotionType == PotionType.Random)
            {
                for (int i = 0; i < quantityOfPotionsInChest; i++)
                {
                    itemData.PotionType = (PotionType)Random.Range(1, 3);
                    GameController.Instance.AddInventoryItem(itemData);
                }
            }
            else
            {
                for (int i = 0; i < quantityOfPotionsInChest; i++)
                {
                    GameController.Instance.AddInventoryItem(itemData);
                }
            }
            Destroy(gameObject);
        }
    }
}
