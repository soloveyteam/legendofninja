﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InventoryItemType { Potion, Amulet }

[System.Serializable]
public class InventoryItem
{
    // Всё перенесено в класс Potion

    //[SerializeField] private CrystallType crystallType;
    //[SerializeField] private float quantity;

    //public CrystallType CrystallType
    //{
    //    get { return crystallType; }
    //    set { crystallType = value; }
    //}

    //public float Quantity
    //{
    //    get { return quantity; }
    //    set { quantity = value; }
    //}

    private InventoryItemType itemType;

    public InventoryItemType ItemType
    {
        get { return itemType; }
        set { itemType = value; }
    }
}

