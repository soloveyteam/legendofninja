﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesTrigger : MonoBehaviour
{
    [SerializeField] private float primaryDamage = 20f;
    [SerializeField] private float secondaryDamage = 1f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StopAllCoroutines();
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();
        if (player != null)
        {
            player.RecieveHit(primaryDamage);
            StartCoroutine(RecieveDamageFromSpikes(player));
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();
        if (player != null)
        {
            StopAllCoroutines();
        }
    }

    private IEnumerator RecieveDamageFromSpikes(PlayerController player)
    {
        while(true)
        {
            yield return new WaitForSeconds(0.5f);
            player.RecieveHit(secondaryDamage);
        }
    }
}
