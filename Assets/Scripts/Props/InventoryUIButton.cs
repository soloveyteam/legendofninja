﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIButton : MonoBehaviour
{
    private InventoryItem itemData;
    //[SerializeField] private Text label;
    //[SerializeField] private Text count;
    [SerializeField] private Image image;
    [SerializeField] private List<Sprite> sprites;
    private InventoryUsedCallback callback;

    public InventoryUsedCallback Callback
    {
        get { return callback; }
        set { callback = value; }
    }

    //public Text Label
    //{
    //    get { return label; }
    //    set { label = value; }
    //}

    //public Text Count
    //{
    //    get { return count; }
    //    set { count = value; }
    //}

    public InventoryItem ItemData
    {
        get { return itemData; }
        set { itemData = value; }
    }

    private void Start()
    {
        if (itemData.ItemType == InventoryItemType.Amulet)
        {
            string spriteNameForAmuletToSearch = ((Amulet)itemData).AmuletType.ToString().ToLower();
            image.sprite = sprites.Find(x => x.name.Contains(spriteNameForAmuletToSearch));
            //Label.text = "Speed";
            //Count.text = "1";
            gameObject.GetComponent<Button>().onClick.AddListener(() => callback(this));
        }

        if (itemData.ItemType == InventoryItemType.Potion)
        {
            string spriteNameToSearch = ((Potion)itemData).PotionType.ToString().ToLower();

            image.sprite = sprites.Find(x => x.name.Contains(spriteNameToSearch));

            //Label.text = spriteNameToSearch;

            //Count.text = ((Potion)itemData).Quantity.ToString();

            gameObject.GetComponent<Button>().onClick.AddListener(() => callback(this));
        }
    }
}
