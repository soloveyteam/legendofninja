﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatformTrigger : MonoBehaviour
{
    [SerializeField] private Rigidbody2D m_rigidbody2D;
    [SerializeField] private GameObject rope;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Fireball fireball = collision.gameObject.GetComponent<Fireball>();
        if (fireball != null)
        {
            Destroy(rope);
            m_rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
            m_rigidbody2D.mass = 1000f;
        }
    }
}
