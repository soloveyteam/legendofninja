﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestWithAmulet : MonoBehaviour
{
    public Amulet itemData = new Amulet();

    private void OnTriggerEnter2D(Collider2D collider)
    {
        PlayerController player = collider.gameObject.GetComponent<PlayerController>();
        if (player != null)
        {
            // Тип амулета задаётся в инспекторе unity.

            itemData.ItemType = InventoryItemType.Amulet;

            GameController.Instance.AddInventoryItem(itemData);

            Destroy(gameObject);
        }
    }
}
