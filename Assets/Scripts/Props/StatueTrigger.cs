﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatueTrigger : MonoBehaviour
{
    private PlayerController player;
    private bool isPlayerReadyToBendOneKnee = false;
    [SerializeField] private GameObject startDialogCanvasOnStatue;

    private void Update()
    {
        if (isPlayerReadyToBendOneKnee)
        {
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                //Вылечить персонажа
                GameController.Instance.LevelUp();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = collision.GetComponent<PlayerController>();
        if (player != null)
        {
            isPlayerReadyToBendOneKnee = true;
            startDialogCanvasOnStatue.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        player = collision.GetComponent<PlayerController>();
        if (player != null)
        {
            isPlayerReadyToBendOneKnee = false;
            startDialogCanvasOnStatue.SetActive(false);
        }
    }
}
