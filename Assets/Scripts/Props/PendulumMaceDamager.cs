﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumMaceDamager : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            IDestructable destructable = collision.gameObject.GetComponent<IDestructable>();
            if (destructable != null)
            {
                destructable.RecieveHit(20f);
            }
        }
    }
}
