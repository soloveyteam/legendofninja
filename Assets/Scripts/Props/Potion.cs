﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Potion : InventoryItem
{
    [SerializeField] private PotionType potionType;
    //[SerializeField] private float quantity;

    public PotionType PotionType
    {
        get { return potionType; }
        set { potionType = value; }
    }

    //public float Quantity
    //{
    //    get { return quantity; }
    //    set { quantity = value; }
    //}
}
