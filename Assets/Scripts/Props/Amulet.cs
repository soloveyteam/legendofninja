﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Amulet : InventoryItem
{
    [SerializeField] private AmuletType amuletType;

    public AmuletType AmuletType
    {
        get { return amuletType; }
        set { amuletType = value; }
    }
}

public enum AmuletType { SpeedIncrease, ManaRecovery }
