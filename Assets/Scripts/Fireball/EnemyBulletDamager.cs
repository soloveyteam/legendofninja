﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletDamager : MonoBehaviour
{
    [SerializeField] private float damage = 50f;
    [SerializeField] private float radius = 0.5f;

    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, radius);
        for (int i = 0; i < hits.Length; i++)
        {
            if (!GameObject.Equals(hits[i].gameObject, gameObject))
            {
                IDestructable destructable = hits[i].gameObject.GetComponent<IDestructable>();
                if (destructable != null)
                {
                    destructable.RecieveHit(Damage);
                }
            }
        }
    }
}
