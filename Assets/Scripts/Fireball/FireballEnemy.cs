﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballEnemy : MonoBehaviour
{
    private Animator m_animator;
    private CircleCollider2D m_circleCollider;
    //private SpriteRenderer m_spriteRenderer;
    private GameObject owner;
    private float speed = 10.0f;
    private Vector3 direction;

    public Vector3 Direction
    {
        set
        {
            direction = value;
        }
    }

    public GameObject Owner
    {
        get { return owner; }
        set { owner = value; }
    }

    void Start()
    {
        m_circleCollider = GetComponent<CircleCollider2D>();
        m_animator = GetComponentInChildren<Animator>();
        //m_spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        //m_spriteRenderer.color = Color.green;
        //if (direction.x > 0.0f)
        //    transform.localScale = Vector3.one;
        //else
        //    transform.localScale = new Vector3(-1, 1, 1);

        Destroy(gameObject, 10.0f);
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDestructable victim = collision.gameObject.GetComponent<IDestructable>();
        if ((Owner != null) && (collision.gameObject != Owner) && (victim != null))
        {
            speed = 0.0f;
            m_circleCollider.enabled = false;
            m_animator.SetTrigger("Collision");
            Destroy(gameObject, GetAnimationLength("Explosion"));
        }
        else if ((victim == null) && ((collision.transform.CompareTag("Wall") || collision.transform.CompareTag("Ground"))))
        {
            speed = 0.0f;
            m_circleCollider.enabled = false;
            m_animator.SetTrigger("Collision");
            Destroy(gameObject, GetAnimationLength("Explosion"));
        }
    }

    private float GetAnimationLength(string clipName)
    {
        // Получить длину анимационного клипа по названию.
        RuntimeAnimatorController animatorController = m_animator.runtimeAnimatorController;
        for (int i = 0; i < animatorController.animationClips.Length; i++)
        {
            if (animatorController.animationClips[i].name == clipName)
            {
                return animatorController.animationClips[i].length;
            }
        }
        return 0.0f;
    }
}
