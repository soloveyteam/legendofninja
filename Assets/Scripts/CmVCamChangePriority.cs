﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CmVCamChangePriority : MonoBehaviour
{
    [SerializeField] CmVCamControlLvl1 camControlLvl1;
    [SerializeField] private int cameraNumber;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            camControlLvl1.RaiseCameraPriority(cameraNumber);
        }
    }
}
