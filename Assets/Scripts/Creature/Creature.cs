﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour, IDestructable
{
    protected Animator m_animator;
    protected Rigidbody2D m_rigidbody;
    private SpriteRenderer spriteRenderer;
    [SerializeField] private SpriteColorController m_spriteColorController;
    [SerializeField] protected float speed;
    [SerializeField] protected float damage;
    [SerializeField] protected float health = 100;

    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public float Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }

    void Awake()
    {
        m_animator = gameObject.GetComponentInChildren<Animator>();
        m_rigidbody = gameObject.GetComponent<Rigidbody2D>();
        
    }
    private void Start()
    {
        //m_spriteColorController = gameObject.GetComponent<SpriteColorController>() as SpriteColorController;
    }

    public virtual void Die()
    {
        GameController.Instance.Killed(this);
    }

    public void RecieveHit(float damage)
    {
        if (!GameController.Instance.IsPlayerDead)
        {
            Health -= damage;
            switch(Random.Range(0, 3))
            {
                case 0:
                    GameController.Instance.AudioManager.PlaySoundRandomPitch("GetDamage");
                    break;
                case 1:
                    GameController.Instance.AudioManager.PlaySoundRandomPitch("GetDamage2");
                    break;
                case 2:
                    GameController.Instance.AudioManager.PlaySoundRandomPitch("GetDamage3");
                    break;
            }
            

            if (m_spriteColorController != null)
                m_spriteColorController.MakeSpriteColorRedOn(0.1f);
            else
                Debug.LogError("m_spriteColorController == null in " + gameObject.name);
            GameController.Instance.Hit(this);
            if (Health <= 0)
            {
                Die();
            }
        }
    }

    protected void DoHit(Vector3 hitPosition, float hitRadius, float hitDamage)
    {
        Collider2D[] hits = Physics2D.OverlapCircleAll(hitPosition, hitRadius);
        for (int i = 0; i < hits.Length; i++)
        {
            if (!GameObject.Equals(hits[i].gameObject, gameObject))
            {
                IDestructable destructable = hits[i].gameObject.GetComponent<IDestructable>();
                if (destructable != null)
                {
                    destructable.RecieveHit(hitDamage);
                }
            }
        }
    }
}
