﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteColorController : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    
    void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    public void MakeSpriteColorRedOn(float time)
    {
        spriteRenderer.color = new Color(0.9245283f, 0.1788003f, 0.1788003f, 1f);
        Invoke("ChangeSpriteColorToWhtie", time);
    }

    private void ChangeSpriteColorToWhtie()
    {
        spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
    }
}
