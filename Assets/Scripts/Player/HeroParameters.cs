﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HeroParameters
{
    #region Private_Variables
    [SerializeField] private float maxHealth = 100f;
    [SerializeField] private float maxMana = 100f;
    [SerializeField] private float damage = 20f;
    [SerializeField] private float speed = 5f;
    [SerializeField] private int experience = 0;
    private int nextExperienceLevel = 100;
    private int previousExperienceLevel = 0;
    private int level = 1;
    private int statPoints = 0;
    [SerializeField] private float maxMaxHealth = 500f;
    [SerializeField] private float maxDamage = 25f;
    [SerializeField] private float maxSpeed = 10f;
    [SerializeField] private float amuletSpeedMultiplier = 1f;
    #endregion

    public int NextExperienceLevel
    {
        get { return nextExperienceLevel; }
        set { nextExperienceLevel = value; }
    }

    public int PreviousExperienceLevel
    {
        get { return previousExperienceLevel; }
        set { previousExperienceLevel = value; }
    }

    public float AmuletSpeedMultiplier
    {
        get { return amuletSpeedMultiplier; }
    }

    public float MaxMaxHealth
    {
        get { return maxMaxHealth; }
    }

    public float MaxDamage
    {
        get { return maxDamage; }
    }

    public float MaxSpeed
    {
        get { return maxSpeed; }
    }

    public int Level
    {
        get { return level; }
        set { level = value; }
    }

    public int StatPoints
    {
        get { return statPoints; }
        set { statPoints = value; }
    }

    public float MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }

    public float MaxMana
    {
        get { return maxMana; }
        set { maxMana = value; }
    }

    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    public int Experience
    {
        get { return experience; }
        set 
        { 
            experience = value;
            CheckExperienceLevel();
        }
    }

    private void CheckExperienceLevel()
    {
        while (experience > nextExperienceLevel)
        {
            level++;
            //рассчет следующего уровня опыта
            int addition = previousExperienceLevel;
            previousExperienceLevel = nextExperienceLevel;
            nextExperienceLevel += addition;

            //-------------------------------------------------------------------------------------
            // Свой код на stat points
            StatPoints += 3;
            GameController.Instance.AudioManager.PlaySound("level_up");
            //-------------------------------------------------------------------------------------

            //Вызов метода в контроллере для обновления
            //HUD и параметров игрока:
            GameController.Instance.LevelUp();
        }
    }
}
