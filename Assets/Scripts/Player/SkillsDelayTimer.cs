﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillsDelayTimer : MonoBehaviour
{
    private float timerForDash;
    private float timerForFireball;
    private float timerForSwordAttack;
    [SerializeField] private float delayForDash = 2.0f;
    [SerializeField] private float delayForFireball = 2.0f;
    [SerializeField] private float delayForSwordAttack = 0f;
    private bool dashUseIsReady;
    private bool fireballUseIsReady;
    private bool swordAttackUseIsReady;

    public bool DashUseIsReady
    {
        get { return dashUseIsReady; }
    }

    public bool FireballUseIsReady
    {
        get { return fireballUseIsReady; }
    }

    public bool SwordAttackUseIsReady
    {
        get { return swordAttackUseIsReady; }
    }

    void Start()
    {
        timerForDash = delayForDash;
        timerForFireball = delayForFireball;
        timerForSwordAttack = delayForSwordAttack;
    }

    void Update()
    {
        if (timerForDash <= delayForDash)
        {
            timerForDash += Time.deltaTime;
            dashUseIsReady = false;
        }
        else
        {
            dashUseIsReady = true;
        }

        if (timerForFireball <= delayForFireball)
        {
            timerForFireball += Time.deltaTime;
            fireballUseIsReady = false;
        }
        else
        {
            fireballUseIsReady = true;
        }

        if (timerForSwordAttack <= delayForSwordAttack)
        {
            timerForSwordAttack += Time.deltaTime;
            swordAttackUseIsReady = false;
        }
        else
        {
            swordAttackUseIsReady = true;
        }
    }

    public void DashWasUsed()
    {
        dashUseIsReady = false;
        timerForDash = 0.0f;
    }

    public void FireballWasUsed()
    {
        fireballUseIsReady = false;
        timerForFireball = 0.0f;
    }

    public void SwordAttackWasUsed()
    {
        swordAttackUseIsReady = false;
        timerForSwordAttack = 0.0f;
    }
}
