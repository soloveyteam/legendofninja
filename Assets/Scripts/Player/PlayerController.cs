﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerController : Creature
{
    [SerializeField] private float jumpVelocity;
    [SerializeField] private float fallMultiplier;
    [SerializeField] private float lowJumpMultiplier;
    [SerializeField] private float crouchDashSpeed;
    [SerializeField] private Transform groundChecker;
    [SerializeField] private Transform attackPoint;
    [SerializeField] private Transform fireballLauncher;
    
    private CapsuleCollider2D m_capsuleCollider2D;
    private ParticleSystem m_dust;
   // private SpriteRenderer m_spriteRenderer;
    private SkillsDelayTimer m_delayTimer;
    private Fireball fireball;
    private bool onGround = true;
    private bool onGroundForAnimator = true; // Чтобы отключать переход в анимацию прыжка ,но не трогать основной onGround
    private bool isPlayerLookingToRight = true;
    private bool isReadyToCrouch = false;
    private bool isDash = false;
    private bool isAnimationInAirOnPlay = false;
    private float playerSpeedBeforeCrouch;
    private float attackRange = 0.5f;
    private const int c_JumpFramesThreshold = 20;
    private int m_FramesFromJumpStart = 0;

    private float CurrentMana
    {
        get { return GameController.Instance.PlayerMana; }
        set { GameController.Instance.PlayerMana = value; }
    }

    private float FireballCostsMana
    {
        get { return GameController.Instance.FireballCostsMana; }
    }

    private void Awake()
    {
        m_animator = GetComponentInChildren<Animator>();
        m_rigidbody = GetComponent<Rigidbody2D>();
        //m_spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        m_delayTimer = GetComponent<SkillsDelayTimer>();
        m_dust = GetComponentInChildren<ParticleSystem>();
        m_capsuleCollider2D = GetComponent<CapsuleCollider2D>();

        // Подгружаем ссылку на префаб "Fireball"
        fireball = Resources.Load<Fireball>("Prefabs/Fireball/Fireball");
    }

    void Start()
    {
        playerSpeedBeforeCrouch = Speed;

        GameController.Instance.Player = this;

        GameController.Instance.OnUpdateHeroParameters += HandleOnUpdateHeroParameters;
        GameController.Instance.OnUpdateHeroCurrentHealth += HandleOnUpdateHeroCurrentHealth;
    }

    void Update()
    {
        if (GameController.Instance.State == GameState.Pause)
        {
            return;
        }

        //onGround = CheckGround();

        if (!onGround)
        {
            m_FramesFromJumpStart++;
        }

        if (!isAnimationInAirOnPlay)
        {
            onGroundForAnimator = onGround;
        }

        Run();
        Jump();
        Flip();
        AnimatorController();
        SwordAttack();
        ShootFireball();
        Crouch();
        Dash();
    }

    private void FixedUpdate()
    {
        onGround = CheckGround();
    }


    private void Run()
    {
        if (!isDash)
        {
            // Если не в Дэше - прикладываем скорость по нажатию клавиш.
            Vector2 velocity = m_rigidbody.velocity;

            velocity.x = Input.GetAxis("Horizontal") * Speed;

            m_rigidbody.velocity = velocity;
        }
        else if (isDash)
        {
            Vector2 velocity = new Vector2(Speed * 5, 0f);

            if (!isPlayerLookingToRight)
                velocity.x *= -1;
            m_rigidbody.velocity = velocity;
        }
    }

    private void Jump()
    {
        //if (Input.GetButtonDown("Jump") && onGround)
        //{
        //    CreateDust();
        //    m_rigidbody.velocity = Vector2.up * jumpVelocity;
        //}

        if ((Input.GetButtonDown("Jump") && onGround) || 
            (Input.GetButtonDown("Jump") && (m_FramesFromJumpStart <= c_JumpFramesThreshold) && (m_rigidbody.velocity.y <= 0)))
        {
            m_FramesFromJumpStart = c_JumpFramesThreshold + 1;
            CreateDust();
            m_rigidbody.velocity = Vector2.up * jumpVelocity;
        }

        if (!isDash)
        {
            if (m_rigidbody.velocity.y < 0)
            {
                m_rigidbody.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }
            else if (m_rigidbody.velocity.y > 0 && !Input.GetButton("Jump"))
            {
                m_rigidbody.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
            }
        }
        else
        {
            Vector2 velocity = new Vector2(m_rigidbody.velocity.x, 0f);
            m_rigidbody.velocity = velocity;
        }
    }

    private void Flip()
    {
        if (!isDash)
        {
            if (transform.localScale.x < 0)
            {
                if (Input.GetAxis("Horizontal") > 0)
                {
                    transform.localScale = Vector3.one;
                    isPlayerLookingToRight = true;
                    if (onGround)
                        CreateDust();
                }
            }
            else
            {
                if (Input.GetAxis("Horizontal") < 0)
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                    isPlayerLookingToRight = false;
                    if (onGround)
                        CreateDust();
                }
            }
        }
    }

    private bool CheckGround()
    {
        RaycastHit2D[] hits = Physics2D.LinecastAll(transform.position + new Vector3(0, -0.8f), groundChecker.position);
        for (int i = 0; i < hits.Length; i++)
        {
            if (!GameObject.Equals(hits[i].collider.gameObject, gameObject) && 
                (hits[i].collider.gameObject.CompareTag("MovingPlatform") ||
                hits[i].collider.gameObject.CompareTag("Ground") ||
                hits[i].collider.gameObject.CompareTag("Platform")))
            {
                // Прицепляемся к движущейся платформе
                if (hits[i].collider.gameObject.CompareTag("MovingPlatform"))
                {
                    transform.SetParent(hits[i].collider.gameObject.transform);
                }

                m_FramesFromJumpStart = 0;

                return true;
            }
        }

        // Отцепляемся от движущейся платформы
        if (transform.parent != null && transform.parent.CompareTag("MovingPlatform"))
        {
            transform.SetParent(null);
        }

        return false;
    }

    private void AnimatorController()
    {
        // Прыжок и падение
        m_animator.SetBool("OnGround", onGroundForAnimator);
        m_animator.SetFloat("VerticalSpeed", m_rigidbody.velocity.y);

        // Бег
        m_animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
    }

    private void SwordAttack()
    {
        if (!GameController.Instance.IsMouseOverUIButton)
        {
            if (m_delayTimer.SwordAttackUseIsReady)
            {
                if (Input.GetButtonDown("Fire1") && onGround)
                {
                    m_animator.SetTrigger("SwordAttackOnGround");
                    m_delayTimer.SwordAttackWasUsed();

                    MakeDamageAtSword();
                }
                else if (Input.GetButtonDown("Fire1") && !onGround)
                {
                    m_animator.SetTrigger("SwordAttackInJump");
                    AllowFullAnimationInAir("Sword_Attack_In_Jump");
                    m_delayTimer.SwordAttackWasUsed();

                    MakeDamageAtSword();
                }
            }
        }
    }

    private void MakeDamageAtSword()
    {
        DoHit(attackPoint.position, attackRange, Damage);
        GameController.Instance.AudioManager.PlaySoundRandomPitch("Sword_strike");
    }

    private void ShootFireball()
    {
        if (m_delayTimer.FireballUseIsReady && (CurrentMana >= FireballCostsMana))
        {
            if (Input.GetButtonDown("Fire2") && !onGround)
            {
                m_animator.SetTrigger("FireballAttackInJump");
                AllowFullAnimationInAir("Fireball_Attack_In_Jump");
                m_delayTimer.FireballWasUsed();
            }
            else if (Input.GetButtonDown("Fire2") && onGround)
            {
                m_animator.SetTrigger("FireballAttackOnGround");
                m_delayTimer.FireballWasUsed();
            }
        }
    }

    public void CreateFireball()
    {
        Vector3 position = fireballLauncher.transform.position;
        Fireball newFireball = Instantiate(fireball, position, fireball.transform.rotation) as Fireball;
        newFireball.Owner = gameObject;
        newFireball.Direction = newFireball.transform.right * (isPlayerLookingToRight ? 1.0f : -1.0f);
        GameController.Instance.SubstractFromManaForFireball();
    }

    private void Crouch()
    {
        if ((Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) && !onGround)
        {
            isReadyToCrouch = true;
        }

        if ((Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow)) && !onGround)
        {
            isReadyToCrouch = false;
        }

        if (isReadyToCrouch && onGround)
        {
            m_animator.SetBool("Crouch", true);
            Speed = 0.0f;

            // Изменяем размеры коллайдера под присед.
            Vector2 tempVector2 = new Vector2();
            tempVector2.x = -0.056f;
            tempVector2.y = -0.1894734f;
            m_capsuleCollider2D.offset = tempVector2;

            tempVector2.x = 0.6f;
            tempVector2.y = 1.171053f;
            m_capsuleCollider2D.size = tempVector2;
        }

        if ((Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) && onGround)
        {
            m_animator.SetBool("Crouch", true);
            Speed = 0.0f;

            // Изменяем размеры коллайдера под присед.
            Vector2 tempVector2 = new Vector2();
            tempVector2.x = -0.056f;
            tempVector2.y = -0.1894734f;
            m_capsuleCollider2D.offset = tempVector2;

            tempVector2.x = 0.6f;
            tempVector2.y = 1.171053f;
            m_capsuleCollider2D.size = tempVector2;
        }
        
        if (Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow) && onGround)
        {
            m_animator.SetBool("Crouch", false);
            isReadyToCrouch = false;
            Speed = playerSpeedBeforeCrouch;

            // Когда встаём возвращаем коллайдеру исходные размеры
            Vector2 tempVector2 = m_capsuleCollider2D.offset;
            tempVector2.x = -0.056f;
            tempVector2.y = -0.025f;
            m_capsuleCollider2D.offset = tempVector2;

            tempVector2.x = 0.6f;
            tempVector2.y = 1.5f;
            m_capsuleCollider2D.size = tempVector2;
        }
    }

    private void Dash()
    {
        if (GameController.Instance.IsAmuletOfSpeedOn)
        {
            if (m_delayTimer.DashUseIsReady)
            {
                if ((Input.GetKeyDown(KeyCode.RightShift) || Input.GetKeyDown(KeyCode.LeftShift)))
                {
                    float time;

                    if (onGround)
                    {
                        m_animator.SetTrigger("DashOnGround");
                        time = GetAnimationLength("Dash_On_Ground");

                        // Изменяем размеры коллайдера под Дэш.
                        Vector2 tempVector2 = new Vector2();
                        tempVector2.x = -0.1955709f;
                        tempVector2.y = -0.3044142f;
                        m_capsuleCollider2D.offset = tempVector2;

                        tempVector2.x = 0.9411716f;
                        tempVector2.y = 0.9411716f;
                        m_capsuleCollider2D.size = tempVector2;
                    }
                    else
                    {
                        m_animator.SetTrigger("DashInJump");
                        AllowFullAnimationInAir("Dash_In_Jump");
                        // Временно убираем гравитацию с rigidbody
                        m_rigidbody.gravityScale = 0.0f;
                        time = GetAnimationLength("Dash_In_Jump");
                        Invoke("ReturnGravityScale", time);
                    }

                    isDash = true;
                    Invoke("OffDash", time);
                    m_delayTimer.DashWasUsed();
                }
            }
        }
    }

    private void OffDash()
    {
        isDash = false;

        // Когда встаём возвращаем коллайдеру исходные размеры
        Vector2 tempVector2 = m_capsuleCollider2D.offset;
        tempVector2.x = -0.056f;
        tempVector2.y = -0.025f;
        m_capsuleCollider2D.offset = tempVector2;

        tempVector2.x = 0.6f;
        tempVector2.y = 1.5f;
        m_capsuleCollider2D.size = tempVector2;
    }

    private float GetAnimationLength(string clipName)
    {
        // Получить длину анимационного клипа по названию.
        RuntimeAnimatorController animatorController = m_animator.runtimeAnimatorController;
        for (int i = 0; i < animatorController.animationClips.Length; i++)
        {
            if (animatorController.animationClips[i].name == clipName)
            {
                return animatorController.animationClips[i].length;
            }
        }
        return 0.0f;
    }

    private void AllowGroundCheckForAnimator()
    {
        // Разрешить отслеживать Аниматору на земле или нет игрок находится.
        isAnimationInAirOnPlay = false;
    }

    private void ReturnGravityScale()
    {
        m_rigidbody.gravityScale = 1.0f;
    }

    // Этот метод необходим, потомучто анимация Jump перекрывает другие
    // анимации в прыжке, так как работает от переменной типа bool. 
    // Здесь мы временно отключаем обратный переход на Jump, пока проигрывается
    // другая анимация в прыжке.
    private void AllowFullAnimationInAir(string animationName)
    {
        // Разрешить проигрывание поной анимации в воздухе.
        isAnimationInAirOnPlay = true;
        onGroundForAnimator = true;
        float time = GetAnimationLength(animationName);
        Invoke("AllowGroundCheckForAnimator", time);
    }

    private void CreateDust()
    {
        m_dust.Play();
    }

    private void OnDestroy()
    {
        GameController.Instance.OnUpdateHeroParameters -= HandleOnUpdateHeroParameters;
        GameController.Instance.OnUpdateHeroCurrentHealth -= HandleOnUpdateHeroCurrentHealth;
    }

    private void HandleOnUpdateHeroParameters(HeroParameters parameters)
    {
        //Health = parameters.MaxHealth;
        Damage = parameters.Damage;
        if (GameController.Instance.IsAmuletOfSpeedOn)
            Speed = parameters.Speed * parameters.AmuletSpeedMultiplier;
        else
            Speed = parameters.Speed;
        playerSpeedBeforeCrouch = parameters.Speed;
        //CurrentMana = parameters.MaxMana;
    }

    private void HandleOnUpdateHeroCurrentHealth(HeroParameters parameters)
    {
        Health = parameters.MaxHealth;
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.transform.CompareTag("MovingPlatform"))
    //    {
    //        transform.SetParent(collision.transform);
    //    }
    //}

    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    if (collision.transform.CompareTag("MovingPlatform"))
    //    {
    //        transform.SetParent(null);
    //    }
    //}

}
