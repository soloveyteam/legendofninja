﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateFireball : MonoBehaviour
{
    private PlayerController playerController;
    void Start()
    {
        playerController = GetComponentInParent<PlayerController>();
    }

    public void InstantiateFireball()
    {
        playerController.CreateFireball();
    }
}
