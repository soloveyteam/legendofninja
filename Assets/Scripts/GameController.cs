﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState { Play, Pause }

// Делегаты
public delegate void InventoryUsedCallback(InventoryUIButton item);
public delegate void UpdateHeroParametersHandler(HeroParameters parameters);

public class GameController : MonoBehaviour
{
    public event UpdateHeroParametersHandler OnUpdateHeroParameters;
    public event UpdateHeroParametersHandler OnUpdateHeroMaxHealth;
    public event UpdateHeroParametersHandler OnUpdateHeroCurrentHealth;
    public event UpdateHeroParametersHandler OnUpdateHeroMana;
    static private GameController _instance;
    private GameState state;
    private int score;
    private int lives = 3;
    private int dragonHitScore = 10;
    private int dragonKillScore = 50;
    private bool isMouseOverUIButton = false;
    private bool isAmuletOfSpeedOn = false;
    private bool isAmuletOfManaRecoveryOn = false;
    private bool isPlayerDead = false;
    [SerializeField] private List<InventoryItem> inventory;
    //[SerializeField] private List<InventoryItem> savedInventory;
    [SerializeField] private Audio audioManager;
    private PlayerController player;
    [SerializeField] private HeroParameters hero;
    private Transform lastSavePoint;
    [SerializeField] private int dragonKillExperience;
    private float playerMana = 100f;
    private float fireballCostsMana = 10f;
    [SerializeField] private DataStore savedDataStore = new DataStore();

    public DataStore SavedDataStore
    {
        get { return savedDataStore; }
    }

    public Transform LastSavePoint
    {
        get { return lastSavePoint; }
        set { lastSavePoint = value; }
    }

    public bool IsPlayerDead
    {
        get { return isPlayerDead; }
        set { isPlayerDead = value; }
    }

    public bool IsAmuletOfSpeedOn
    {
        get { return isAmuletOfSpeedOn; }
        set { isAmuletOfSpeedOn = value; }
    }

    public bool IsAmuletOfManaRecoveryOn
    {
        get { return isAmuletOfManaRecoveryOn; }
        set 
        { 
            if (value == true)
            {
                isAmuletOfManaRecoveryOn = value;
                StartCoroutine("CoroutineRegenMana");
            }
            else
            {
                isAmuletOfManaRecoveryOn = false;
                StopCoroutine("CoroutineRegenMana");
                
            }
            
        }
    }

    public float PlayerMana
    {
        get { return playerMana; }
        set 
        { 
            playerMana = value;
            HUD.Instance.UpdateCurrentMana(value);
        }
    }

    public float FireballCostsMana
    {
        get { return fireballCostsMana; }
    }

    public bool IsMouseOverUIButton
    {
        get { return isMouseOverUIButton; }
        set { isMouseOverUIButton = value; }
    }

    public HeroParameters Hero
    {
        get { return hero; }
        set { hero = value; }
    }

    public Audio AudioManager
    {
        get { return audioManager; }
        set { audioManager = value; }
    }

    public PlayerController Player
    {
        get { return player; }
        set { player = value; }
    }

    private int Score
    {
        get
        {
            return score;
        }
        set
        {
            if (value != score)
            {
                score = value;
                HUD.Instance.SetScore(score.ToString());
            }
        }
    }

    public int Lives
    {
        get { return lives; }
        set 
        { 
            if (value < lives)
            {
                if (value > 0)
                {
                    lives = value;
                    HUD.Instance.SetLivesValue(lives);
                    LostOneLife();
                }
                else
                {
                    lives = value;
                    GameOver();
                }
            }
            else
            {
                lives = value;
            }
            
        }
    }

    public List<InventoryItem> Inventory
    {
        get { return inventory; }
        set { inventory = value; }
    }

    public GameState State
    {
        get { return state; }
        set 
        {
            if (value == GameState.Play)
            {
                Time.timeScale = 1.0f;
            }
            else
            {
                Time.timeScale = 0.0f;
            }
            state = value; 
        }
    }

    public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gameController = Instantiate(Resources.Load("Prefabs/GameController")) as GameObject;
                _instance = gameController.GetComponent<GameController>();
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            if (_instance != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);

        InitializeAudioManager();
        state = GameState.Play;
        inventory = new List<InventoryItem>();
        //Debug.Log("Сохраняем текущий инвентарь.");
        //savedInventory = inventory;
        savedDataStore.SaveInventory();
        savedDataStore.SaveHeroParameters();
    }

    public void StartNewLevel()
    {
        HUD.Instance.SetScore(Score.ToString());
        HUD.Instance.SetLivesValue(Lives);
        if (OnUpdateHeroParameters != null)
        {
            OnUpdateHeroParameters(hero);
        }
        if (OnUpdateHeroMaxHealth != null)
        {
            OnUpdateHeroMaxHealth(hero);
        }
        if (OnUpdateHeroCurrentHealth != null)
        {
            OnUpdateHeroCurrentHealth(hero);
        }
        if (OnUpdateHeroMana != null)
        {
            OnUpdateHeroMana(hero);
        }
        State = GameState.Play;
    }

    public void Hit(IDestructable victim)
    {
        if (victim.GetType() == typeof(DragonController))
        {
            Score += dragonHitScore;
        }

        if (victim.GetType() == typeof(PlayerController))
        {
            HUD.Instance.HealthBar.value = victim.Health;
        }

    }

    public void SubstractFromManaForFireball()
    {
        PlayerMana -= fireballCostsMana;
    }

    public void Killed(IDestructable victim)
    {
        if (victim.GetType() == typeof(DragonController))
        {
            Score += dragonKillScore;
            hero.Experience += dragonKillExperience;
            Destroy((victim as MonoBehaviour).gameObject);
        }

        if (victim.GetType() == typeof(BossHpController))
        {
            Destroy((victim as MonoBehaviour).gameObject.transform.root.gameObject);
        }

        if (victim.GetType() == typeof(PlayerController))
        {
            IsPlayerDead = true;
            player.gameObject.transform.SetParent(null);
            Lives -= 1;
            //GameOver();
        }
    }

    public void PlayerTeleportationToLastSavePoint()
    {
        player.GetComponent<Transform>().position = LastSavePoint.position;
        LevelUp();
        IsPlayerDead = false;
    }

    public void AddInventoryItem(InventoryItem itemData)
    {
        InventoryUIButton newUiButton = HUD.Instance.AddNewInventoryItem(itemData);
        InventoryUsedCallback callback = new InventoryUsedCallback(InventoryItemUsed);
        newUiButton.Callback = callback;
        inventory.Add(itemData);

    }

    public void InventoryItemUsed(InventoryUIButton item)
    {
        // Нужно сделать условие разделения действий.
        // Если это зелье, то делать что ниже
        // если амулет, то удалить его из инвентаря и надеть
        if (item.ItemData.ItemType == InventoryItemType.Potion)
        {
            switch (((Potion)item.ItemData).PotionType)
            {
                case PotionType.ManaPotion:
                    //hero.Speed += ((Potion)item.ItemData).Quantity / 10f;

                    if (OnUpdateHeroMana != null)
                    {
                        OnUpdateHeroMana(hero);
                    }

                    playerMana = hero.MaxMana;

                    break;

                case PotionType.HealthPotion:
                    //hero.Damage += ((Potion)item.ItemData).Quantity / 10f;

                    if (OnUpdateHeroCurrentHealth != null)
                    {
                        OnUpdateHeroCurrentHealth(hero);
                    }

                    break;

                default:
                    Debug.LogError("Wrong potion type!");
                    break;
            }

            inventory.Remove(item.ItemData);
            Destroy(item.gameObject);
        }

        // Если юзнули амулет.
        if (item.ItemData.ItemType == InventoryItemType.Amulet)
        {
            HUD.Instance.AmuletWasUsed(item.ItemData);

            //HUD.Instance.UpdateCharacterValues(Player.Health, Player.Speed, Player.Damage);
            if (OnUpdateHeroParameters != null)
            {
                OnUpdateHeroParameters(hero);
            }
        }
    }

    public void LoadNextLevel()
    {
        //DataStore.SaveInventory();
        int sceneNumber = SceneManager.GetActiveScene().buildIndex + 1;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1,
        LoadSceneMode.Single);

        AudioManager.PlayMusicLvlNumber(sceneNumber);
    }
    public void RestartLevel()
    {
        Debug.Log("Загружаем сохранённый инвентарь.");
        //inventory = savedInventory;
        savedDataStore.LoadSavedInventory();
        savedDataStore.LoadSavedHeroParameters();

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex,
        LoadSceneMode.Single);

        Lives = 3;
        isPlayerDead = false;
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        Lives = 3;
        isPlayerDead = false;
        IsAmuletOfManaRecoveryOn = false;
        IsAmuletOfSpeedOn = false;
    }

    public void PrincessFound()
    {
        //HUD.Instance.ShowLevelWonWindow();
        LoadNextLevel();
    }

    private void LostOneLife()
    {
        HUD.Instance.ShowDarkScreenForTeleportation();
    }

    public void GameOver()
    {
        HUD.Instance.ShowLevelLoseWindow();
    }

    private void InitializeAudioManager()
    {
        audioManager.SourceSFX = gameObject.AddComponent<AudioSource>();
        audioManager.SourceMusic = gameObject.AddComponent<AudioSource>();
        audioManager.SourceRandomPitchSFX = gameObject.AddComponent<AudioSource>();
        //gameObject.AddComponent<AudioListener>();
    }

    public void LevelUp()
    {
        if (OnUpdateHeroParameters != null)
        {
            OnUpdateHeroParameters(hero);
        }

        if (OnUpdateHeroMana != null)
        {
            OnUpdateHeroMana(hero);
        }

        playerMana = hero.MaxMana;

        if (OnUpdateHeroMaxHealth != null)
        {
            OnUpdateHeroMaxHealth(hero);
        }

        if (OnUpdateHeroCurrentHealth != null)
        {
            OnUpdateHeroCurrentHealth(hero);
        }
    }

    public void HeroParameterWasIncreased(string parameterName)
    {
        float valueOfIncrease = 1f;

        if (hero.StatPoints > 0)
        {
            switch (parameterName)
            {
                case "Damage":
                    if (hero.Damage < hero.MaxDamage)
                    {
                        hero.Damage += valueOfIncrease;
                        hero.StatPoints -= 1;

                        if (hero.Damage >= hero.MaxDamage)
                            HUD.Instance.LockIncreaseButton(parameterName);
                    }
                    break;

                case "Speed":
                    if (hero.Speed < hero.MaxSpeed)
                    {
                        hero.Speed += valueOfIncrease / 10.0f;
                        hero.StatPoints -= 1;

                        if (hero.Speed >= hero.MaxSpeed)
                            HUD.Instance.LockIncreaseButton(parameterName);
                    }
                    break;

                case "MaxHealth":
                    if (hero.MaxHealth < hero.MaxMaxHealth)
                    {
                        hero.MaxHealth += valueOfIncrease * 5.0f;
                        if (OnUpdateHeroMaxHealth != null)
                        {
                            OnUpdateHeroMaxHealth(hero);
                        }
                        hero.StatPoints -= 1;

                        if (hero.MaxHealth >= hero.MaxMaxHealth)
                            HUD.Instance.LockIncreaseButton(parameterName);
                    }
                    break;

                default:
                    Debug.LogError("Wrong parameret in void HeroParameterWasIncreased() (GameController)");
                    break;
            }

            if (OnUpdateHeroParameters != null)
            {
                OnUpdateHeroParameters(hero);
            }
        }
    }

    public void KillNpc(string npcName)
    {
        if (npcName != "")
        {
            GameObject npc = new GameObject();

            switch (npcName)
            {
                case "Призрак":
                    npc = GameObject.Find("Ghost");
                    break;
            }

            //if (npcName == "Призрак")
            //{
            //    npc = GameObject.Find("Ghost");
            //}

            if (npc != null)
            {
                Destroy(npc);
            }
        }
    }

    private IEnumerator CoroutineRegenMana()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            if (PlayerMana < hero.MaxMana)
                PlayerMana += 1f;
        }
    }
}
